// START BÀI TẬP 1
function findN() {
  //   console.log("yes");
  var n1 = document.getElementById("txt-ex01-n1").value * 1;
  var sumN = 0;
  for (var countN = 1; sumN <= n1; countN++) {
    sumN += countN;
  }
  countN--;

  document.getElementById(
    "ex01-result"
  ).innerHTML = `Số nguyên <span class="text-danger fw-semibold">n</span>=<span class="text-warning fw-semibold">${countN}</span> thỏa điều kiện bài toán.`;
}
// END BÀI TẬP 1

// START BÀI TẬP 2
function calcSn() {
  //   console.log("yes");
  var ex02n = document.getElementById("txt-ex02-n").value * 1;
  var ex02x = document.getElementById("txt-ex02-x").value * 1;
  var sumTotalXpowN = 0;
  for (var ex02Count = 1; ex02Count <= ex02n; ex02Count++) {
    sumTotalXpowN += Math.pow(ex02x, ex02Count);
  }

  document.getElementById(
    "ex02-result"
  ).innerHTML = `Tổng <span class="text-danger fw-semibold">S(n)</span> = <span class="text-warning fw-semibold">${sumTotalXpowN}</span>`;
}
// END BÀI TẬP 2

// START BÀI TẬP 3
function tinhGiaiThua() {
  //   console.log("yes");
  var ex03n = document.getElementById("txt-ex03-n").value * 1;
  var tongGiaiThua = 1;
  for (var ex03Count = 1; ex03Count <= ex03n; ex03Count++) {
    tongGiaiThua *= ex03Count;
  }

  document.getElementById(
    "ex03-result"
  ).innerHTML = `Kết quả: <span class="text-danger fw-semibold">n!</span> = <span class="text-warning fw-semibold">${tongGiaiThua}</span>`;
}
// END BÀI TẬP 3

// START BÀI 4
function inTheDivEx04() {
  //   console.log("yes");
  var soTheDiv = document.getElementById("txt-ex04-so-the").value * 1;
  var totalDiv = "";
  for (var countEx04 = 1; countEx04 <= soTheDiv; countEx04++) {
    if (countEx04 % 2 == 1) {
      totalDiv += `<div class="bg-primary mb-1">${countEx04}</div>`;
    } else {
      totalDiv += `<div class="bg-danger mb-1">${countEx04}</div>`;
    }
  }

  document.getElementById("ex04-result").innerHTML = totalDiv;
}
// END BÀI 4

// START BÀI 5
function inSoNguyenTo() {
  // kiểm tra số nguyên tố
  var kiemTraSoNguyenTo = function (n) {
    if (n <= 1) {
      return false;
    } else {
      // for (var count = 2; n % count != 0; count++);
      // return count == n;
      for (var count = 2; count <= Math.sqrt(n); count++) {
        if (n % count == 0) {
          return false;
        }
      }
    }
    return true;
  };
  // console.log("kiemtra", kiemTraSoNguyenTo(119));
  var ex05n = document.getElementById("txt-ex05-n").value * 1;
  var ex05Result = "";
  for (var ex05Count = 2; ex05Count <= ex05n; ex05Count++) {
    if (kiemTraSoNguyenTo(ex05Count)) {
      ex05Result += ex05Count + " ";
    }
  }

  document.getElementById(
    "ex05-result"
  ).innerHTML = `<span class="fw-semibold text-warning">Dãy số:</span>
  ${ex05Result}`;
}
// END BÀI 5
